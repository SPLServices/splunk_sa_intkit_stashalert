import random
import time
import json
from splunk.util import mktimegm
from splunk.clilib.bundle_paths import make_splunkhome_path

# encoding = utf-8

def process_event(helper, *args, **kwargs):
    """
    # IMPORTANT
    # Do not remove the anchor macro:start and macro:end lines.
    # These lines are used to generate sample code. If they are
    # removed, the sample code will not be updated when configurations
    # are updated.

    [sample_code_macro:start]

    # The following example gets and sets the log level
    helper.set_log_level(helper.log_level)

    # The following example gets the alert action parameters and prints them to the log
    stash_file_group = helper.get_param("stash_file_group")
    helper.log_info("stash_file_group={}".format(stash_file_group))

    sourcetype = helper.get_param("sourcetype")
    helper.log_info("sourcetype={}".format(sourcetype))

    index = helper.get_param("index")
    helper.log_info("index={}".format(index))

    source = helper.get_param("source")
    helper.log_info("source={}".format(source))

    host = helper.get_param("host")
    helper.log_info("host={}".format(host))

    fields = helper.get_param("fields")
    helper.log_info("fields={}".format(fields))

    include_raw = helper.get_param("include_raw")
    helper.log_info("include_raw={}".format(include_raw))

    use_current_time_for_event = helper.get_param("use_current_time_for_event")
    helper.log_info("use_current_time_for_event={}".format(use_current_time_for_event))


    # The following example adds two sample events ("hello", "world")
    # and writes them to Splunk
    # NOTE: Call helper.writeevents() only once after all events
    # have been added
    helper.addevent("hello", sourcetype="sample_sourcetype")
    helper.addevent("world", sourcetype="sample_sourcetype")
    helper.writeevents(index="summary", host="localhost", source="localhost")

    # The following example gets the events that trigger the alert
    events = helper.get_events()
    for event in events:
        helper.log_info("event={}".format(event))

    # helper.settings is a dict that includes environment configuration
    # Example usage: helper.settings["server_uri"]
    helper.log_info("server_uri={}".format(helper.settings["server_uri"]))
    [sample_code_macro:end]
    """

    helper.set_log_level(helper.log_level)
    
    helper.log_info("Alert action stash_event started.")

    source = helper.get_param("source")
    sourcetype = helper.get_param("sourcetype")
    host = helper.get_param("host")
    index = helper.get_param("index")
    stash_file_group = helper.get_param("stash_file_group")

    fields = helper.get_param("fields")
    helper.log_info("fields={}".format(fields))
    
    field_keys = fields.split(' ')
    if fields:
        if "_time" in field_keys:
            del field_keys["_time"]
        if "_raw" in field_keys:
            del field_keys["_raw"]
    events = helper.get_events()

    include_raw = helper.get_param("include_raw")
    helper.log_info("include_raw={}".format(include_raw))
    
    use_current_time_for_event = helper.get_param("use_current_time_for_event")
    helper.log_info("use_current_time_for_event={}".format(use_current_time_for_event))

    DEFAULT_HEADER  = '***SPLUNK***'
    if index:
        DEFAULT_HEADER +=' index=' + index
    if source:
        DEFAULT_HEADER +=' source=' + source
    if sourcetype:
        DEFAULT_HEADER +=' sourcetype=' + sourcetype
    DEFAULT_HEADER +='\n'

    DEFAULT_BREAKER = '==##~~##~~  1E8N3D4E6V5E7N2T9 ~~##~~##==\n'

    ## set up file name and path
    fn = '%s_%s.stash__%s%s' % (mktimegm(time.gmtime()), random.randint(0, 100000), stash_file_group,".stash_new")
    fp = make_splunkhome_path(['var', 'spool', 'splunk', fn])

    fout  = DEFAULT_HEADER  
    for event in events:
        fout += DEFAULT_BREAKER
        if "_time" in event:
            if not use_current_time_for_event:
                fout += event["_time"]
            del event["_time"]
                
        if fields:
            for key in field_keys:
                if key in event:
                    value = event[key]
                    fout += ', %s="%s"' % (key,value)
        else:
            for key,value in event.iteritems():
                if key.startswith("__mv_") != True and not key in ('rid','index', 'source', 'sourcetype','_raw'):
                    fout += ', %s="%s"' % (key,value) 
        if include_raw:
            fout += ', %s="%s"' % ("orig_raw",event["_raw"]) 
    fout.rstrip('\n')


    try:
        with open(fp, 'w') as fh:
            fh.write(fout)
    except Exception as e:
        helper.log_error("Could not write to summary index")
        helper.log_debug("Alert action stash_event ended.")
        helper.writeevents(index=index)
        return 2

    helper.log_debug("Alert action stash_event ended.")
    helper.writeevents(index=index)
    
    # TODO: Implement your alert action logic here
    return 0
