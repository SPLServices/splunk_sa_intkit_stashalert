
[stash_event]
param.index = <string> index. It's a required parameter. It's default value is main.
param.use_current_time_for_event = <bool> Use current time for event.
param.sourcetype = <string> sourcetype. It's a required parameter. It's default value is stash.
param._cam = <json> Active response parameters.
param.host = <string> host.
param.stash_file_group = <string> Stash File Group. It's a required parameter. It's default value is stash_alert.
param.fields = <string> fields.
param.source = <string> source.
param.include_raw = <bool> Include Raw.

